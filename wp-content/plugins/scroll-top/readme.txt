=== Scroll Top ===
Contributors: satrya, themephe
Donate link: http://satrya.me/donate/
Tags: scroll top, back to top, button, to top, jquery, scroll to top
Requires at least: 3.6
Tested up to: 3.9.1
Stable tag: 0.4
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Automaticlly adds a flexible Scroll to Top button to your WordPress website. You can choose color you like, Icon Font or plain text and much more.

== Description ==

This plugin will enable a custom and flexible Scroll/Back To Top to your WordPress website. It comes with unlimited color choices, icon font option to support retina device, hide on screen width smaller than 600 pixels. [Scroll Top Plugin](http://satrya.me/wordpress-plugins/scroll-top/) page info.

= Features Include: =

* WordPress 3.9.1 Support.
* Unlimited colors.
* Retina support(icon font).
* Choose text or icon font.
* Customizable text.
* Position switcher(left or right).
* Change animation you like.
* Automatically hide on screen width smaller than 600 pixels.
* Custom CSS area

= To Do =
* Add more icon fonts

= Plugin Info =
* Developed by [Satrya](http://satrya.me/)
* Check out the [Github](https://github.com/satrya/scroll-top) repo to contribute.

= Donations =
I spent most of my free time maintaining and supporting my free themes and plugins. I will really appreciate if you could spare a couple of bucks to help me to pay my web hosting bill. [Donate here](http://satrya.me/donate/)

**[Rate/Review the plugin](http://wordpress.org/support/view/plugin-reviews/scroll-top) if you find this plugin useful.**

== Installation ==

**Through Dashboard**

1. Log in to your WordPress admin panel and go to Plugins -> Add New
2. Type **scroll top** in the search box and click on search button.
3. Find Scroll Top plugin.
4. Then click on Install Now after that activate the plugin.
5. Go to Settings -> Scroll Top.

**Installing Via FTP**

1. Download the plugin to your hardisk.
2. Unzip.
3. Upload the "scroll-top" folder into your plugins directory.
4. Log in to your WordPress admin panel and click the Plugins menu.
5. Then activate the plugin.
6. Go to Settings -> Scroll Top.

== Screenshots ==

1. The plugin settings.
2. Scroll to top in action.

== Changelog ==

= 0.4 - 7/13/2014 =
* Add custom css box
* Update jquery plugin
* Update language

= 0.3 - 4/22/2014 =
* Update Language
* Add Russian translation
* Update the jQuery plugin