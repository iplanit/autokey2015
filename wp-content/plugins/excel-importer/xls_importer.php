<?php
/*
Plugin Name: XLS Importer
Description: Import data as posts from a XLS file.
Version: 1.0
Author: iPLANiT Ltd.
*/

    
class XLSImporterPlugin {
    
    // Plugin's interface
    function form() {
        
?>

<style>
table.excel {
    border-style:ridge;
    border-width:1;
    border-collapse:collapse;
    font-family:sans-serif;
    font-size:12px;
}
table.excel td {
    vertical-align:bottom;
}
table.excel td {
    padding: 5px 10px;
    border: 1px solid #EEEEEE;
}
</style>

<div class="wrap">
    <h2>XLS Importer</h2>
    <form method="post" enctype="multipart/form-data">

        <!-- File input -->
        <p><label for="xls_import">Upload file:</label><br/>
            <input name="xls_import" id="xls_import" type="file" value="" aria-required="true" /></p>
        <p class="submit"><input type="submit" class="button" name="submit" value="Import" /></p>
    </form>


<?php
        // end form HTML }}}
        if($_REQUEST["submit"]) {
            //$term_id = wp_insert_category(array('cat_name' => $category ));
            
            if ($_FILES['xls_import']['tmp_name']) {

                echo "<h2>Report:</h2>";

                require_once 'excel_reader2.php';
                $data = new Spreadsheet_Excel_Reader($_FILES['xls_import']['tmp_name'], false);
                echo $data->dump();

            } else { echo "No file selected"; }
            
            
        }
        else
        {echo "<a href=\"".get_bloginfo("url")."/wp-content/plugins/excel-importer/AutokeySampleTemplate_Make_Model.xls\" title=\"Download an XLS template\">Download a XLS template</a>";}
        
        ?>
        
        
</div><!-- end wrap -->
        
        
        
        <?

}

    // Handle POST submission
    function post($options) {
        if (empty($_FILES['xls_import']['tmp_name'])) {
            $this->log['error'][] = 'No file uploaded, aborting.';
            $this->print_messages();
            return;
        }

        $time_start = microtime(true);
        $xls = new File_CSV_DataSource;
        $file = $_FILES['xls_import']['tmp_name'];
        $this->stripBOM($file);

        if (!$xls->load($file)) {
            $this->log['error'][] = 'Failed to load file, aborting.';
            $this->print_messages();
            return;
        }

        // pad shorter rows with empty values
        $xls->symmetrize();

        // WordPress sets the correct timezone for date functions somewhere
        // in the bowels of wp_insert_post(). We need strtotime() to return
        // correct time before the call to wp_insert_post().
        $tz = get_option('timezone_string');
        if ($tz && function_exists('date_default_timezone_set')) {
            date_default_timezone_set($tz);
        }

        $skipped = 0;
        $imported = 0;
        $comments = 0;
        foreach ($xls->connect() as $xls_data) {
            if ($post_id = $this->create_post($xls_data, $options)) {
                $imported++;
                $comments += $this->add_comments($post_id, $xls_data);
                $this->create_custom_fields($post_id, $xls_data);
            } else {
                $skipped++;
            }
        }

        if (file_exists($file)) {
            @unlink($file);
        }

        $exec_time = microtime(true) - $time_start;

        if ($skipped) {
            $this->log['notice'][] = "<b>Skipped {$skipped} posts (most likely due to empty title, body and excerpt).</b>";
        }
        $this->log['notice'][] = sprintf("<b>Imported {$imported} posts and {$comments} comments in %.2f seconds.</b>", $exec_time);
        $this->print_messages();
    }

    function create_post($data, $options) {
        extract($options);

        $data = array_merge($this->defaults, $data);
        
		$new_post = array(
            'post_title' => convert_chars($data['xls_post_title']),
            'post_content' => wpautop(convert_chars($data['xls_post_post'])),
            'post_status' => $opt_draft,
            'post_date' => $this->parse_date($data['xls_post_date']),
            'post_excerpt' => convert_chars($data['xls_post_excerpt']),
            'post_name' => $data['xls_post_slug'],
            'post_author' => $this->get_auth_id($data['xls_post_author']),
            'tax_input' => $this->get_taxonomies($data),
            'post_parent' => $data['xls_post_parent'],
        );

        // pages don't have tags or categories
        if ('page' !== $type) {
            $new_post['tags_input'] = $data['xls_post_tags'];

            // Setup categories before inserting - this should make insertion
            // faster, but I don't exactly remember why :) Most likely because
            // we don't assign default cat to post when xls_post_categories
            // is not empty.
            $cats = $this->create_or_get_categories($data, $opt_cat);
            $new_post['post_category'] = $cats['post'];
        }

        // create!
        $id = wp_insert_post($new_post);

        if ('page' !== $type && !$id) {
            // cleanup new categories on failure
            foreach ($cats['cleanup'] as $c) {
                wp_delete_term($c, 'category');
            }
        }
        return $id;
    }

    /**
     * Return an array of category ids for a post.
     *
     * @param string $data xls_post_categories cell contents
     * @param integer $common_parent_id common parent id for all categories
     *
     * @return array() category ids
     */
    function create_or_get_categories($data, $common_parent_id) {
        $ids = array(
            'post' => array(),
            'cleanup' => array(),
        );
        $items = array_map('trim', explode(',', $data['xls_post_categories']));
        foreach ($items as $item) {
            if (is_numeric($item)) {
                if (get_category($item) !== null) {
                    $ids['post'][] = $item;
                } else {
                    $this->log['error'][] = "Category ID {$item} does not exist, skipping.";
                }
            } else {
                $parent_id = $common_parent_id;
                // item can be a single category name or a string such as
                // Parent > Child > Grandchild
                $categories = array_map('trim', explode('>', $item));
                if (count($categories) > 1 && is_numeric($categories[0])) {
                    $parent_id = $categories[0];
                    if (get_category($parent_id) !== null) {
                        // valid id, everything's ok
                        $categories = array_slice($categories, 1);
                    } else {
                        $this->log['error'][] = "Category ID {$parent_id} does not exist, skipping.";
                        continue;
                    }
                }
                foreach ($categories as $category) {
                    if ($category) {
                        $term = is_term($category, 'category', $parent_id);
                        if ($term) {
                            $term_id = $term['term_id'];
                        } else {
                            $term_id = wp_insert_category(array(
                                'cat_name' => $category,
                                'category_parent' => $parent_id,
                            ));
                            $ids['cleanup'][] = $term_id;
                        }
                        $parent_id = $term_id;
                    }
                }
                $ids['post'][] = $term_id;
            }
        }
        return $ids;
    }

    

    function get_auth_id($author) {
        if (is_numeric($author)) {
            return $author;
        }
        $author_data = get_userdatabylogin($author);
        return ($author_data) ? $author_data->ID : 0;
    }


}


function xls_admin_menu() {
    require_once ABSPATH . '/wp-admin/admin.php';
    $plugin = new XLSImporterPlugin;
    add_management_page('edit.php', 'XLS Importer', 9, __FILE__, array($plugin, 'form'));
}

add_action('admin_menu', 'xls_admin_menu');

?>
