<?php
get_header(); 
wp_reset_query();
while( have_posts() ) : the_post();
$thispage=$post->ID;
?>
	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section" class="fixed" style="margin-top:0px"></section>	
	<?php } ?>
	<!-- top section end -->
		

	<?php
    // additional options
    $display_content = get_field('display_main_content' ,$post->ID);
	$display_clients = get_field('display_clients' ,$post->ID);
	$display_contact_link = get_field('display_contact_link' ,$post->ID); 
	?>
		
	<!-- navigation start -->
	<section id="navigation">
		<div class="container">
            <ul class="tabs-navigation left">
				<li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" title="<?php echo get_bloginfo( 'name' ); ?>"/></a></li>
         	
		
			
			</ul>
            
            <ul class="tabs-navigation right">

				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
			</ul>
        
       	</div>        
        
	</section>
    <div class="sub-navigation">
    	<div class="container">
    		<div class="left"><?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?></div>
            <div class="right"><?php dynamic_sidebar('phone-number'); ?></div>
        </div>
        <a href="#" class="bttn hide" title="Close">X</a>
        <a href="#" class="bttn show" title="Show">\/</a>
    </div>
    

	<!-- navigation end -->
	
    <div class="empty-space"></div>
    <!-- Side Navigation starts  -->
        <?php include('side-navigation.php'); ?>
   <!-- Side Navigation ends-->
    <!-- page content -->
    <?php if ($display_content) { ?>
      <section id="content" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
          <div class="container">
              <div class="col-content">
                  <h2><?php the_title(); ?></h2>
                  <?php the_content(); ?>
              </div>
          </div>
      </section>
	<?php } else { ?>
    
    <?php } ?>	

<?php

// check if the flexible content field has rows of data
if( have_rows('section') ):
	$noo = 0;
	$el = 0;
    // loop through the rows of data
    while ( have_rows('section') ) : the_row();
    
	    
/////////////////////////////////////////////////////////////////////////////////////
// TEXT
		
		if( get_row_layout() == 'text' ):
			
			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			// TEXT LAYOUT start
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			// bg image
			$background_image = get_sub_field('background_image');
			if ($background_image) {
				$css_bg = 'style="background:url('.$background_image['url'].') no-repeat;background-size:cover; background-position:center;"';
			}

			// image / text aligment 
			$image_aligment = get_sub_field('image_aligment');
			$add_sub_pages = get_sub_field('add_sub_pages');
			if($image_aligment == 'left') {
				$content_aligment = 'right';
			} else {
				$content_aligment ='left';
			}
			
			$css_txt ='';
			
			// SECTION start
			?>
			<section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> ptb120" <?php echo $css_bg; ?> >
				<div class="container">
				<?php if (get_sub_field('image_')) { ?>
                    <div class="col-half <?php echo $image_aligment; ?>">
                    <figure class="<?php echo 'align_'.$image_aligment; ?>">
                        <?php $img_side = get_sub_field('image_'); ?>
                        <img src="<?php echo $img_side['url']; ?>" alt="<?php echo get_sub_field('title'); ?>" title="<?php echo get_sub_field('title'); ?>">
                    </figure>
                    </div>
                    <div class="col-half <?php echo $content_aligment; ?>" <?php echo $css_txt; ?> >
                        <?php if(!get_sub_field('hide_title')) { echo "<h2>".get_sub_field('title')."</h2>"; } ?>
                        <?php echo get_sub_field('content'); ?>
                        
                        <?php 
                            $buttons  = get_sub_field('buttons');
                            if($buttons) { 
                            	foreach($buttons as $btn){
									if ($btn['button_type'] != 'regular') { ?>
                                       	<?php
                                    	$icon = ($btn['button_type'] == 'play') ? '<span class="fa fa-caret-right"></span>' : '<span class="fa fa-quote-right"></span>'; 
										$pop_txt = $btn['pop-up_text'];
										$pop_vid = $btn['pop-up_video'];
										$pop_tit = ($btn['pop-up_title'])? $btn['pop-up_title'] : $btn['button_label'];
										$noo++;
										?>
										<a class="fancybox button-link play <?php echo $btn['button_type'];?>" title="<?php echo $pop_tit; ?>" href="#contentid<?php echo $noo;?>"> <?php echo $icon; ?> </a>
										<?php
                                        if($pop_vid || $pop_txt) {?>
											<div style="display:none; height:auto !important" id="contentid<?php echo $noo;?>" class="attachvideo">
                                				<?php if ($pop_txt){ echo "<p>$pop_txt</p>"; } ?>
                                				<?php if ($pop_vid) {?>
                               						<iframe src="<?php echo $pop_vid;?>" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												<?php } ?>
                                				<span class="signature"><?php echo $pop_tit;?></span>
                                            </div>
										<?php } ?>
									<?php } else { ?>
                                                                                
                                                                                
<?php //added to use different way to select a link
$posts = $btn['button_text'];

if( $posts ): ?>

    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); 

             $btnlinka = get_permalink();

    endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; 

if ($btn['outbound_link']) {
    if (($btn['the_outbound_link']) !='' ){
         $btnlinka = $btn['the_outbound_link'];
    }
}  
if ($btn['new_window']){
    $newwindow = '_blank';
} else {
    $newwindow = '';
}?>

            
            
										<a class="button-link" title="<?php echo $btn['button_label']; ?>" href="<?php echo $btnlinka; //echo $btn['button_text']; ?>" target="<?php echo $newwindow;?>"> <?php echo $btn['button_label']; ?></a>                                        
									<?php } ?>
								<?php }
                        	} ?>
                    </div>
                <?php } else { ?>
                <div class="col-full" <?php echo $css_txt; ?>>
					<?php if(!get_sub_field('hide_title')) { echo "<h2>".get_sub_field('title')."</h2>"; } ?>
                    
					<?php echo get_sub_field('content'); ?>
					
					<?php 
                            $buttons  = get_sub_field('buttons');
                            if($buttons) { 
                            	foreach($buttons as $btn){
									if ($btn['button_type'] != 'regular') { ?>
                                       	<?php
                                    	$icon = ($btn['button_type'] == 'play') ? '<span class="fa fa-caret-right"></span>' : '<span class="fa fa-quote-right"></span>'; 
										$pop_txt = $btn['pop-up_text'];
										$pop_vid = $btn['pop-up_video'];
										$pop_tit = ($btn['pop-up_title'])? $btn['pop-up_title'] : $btn['button_label'];
										$noo++;
										?>
										<a class="fancybox button-link play <?php echo $btn['button_type'];?>" title="<?php echo $pop_tit; ?>" href="#contentid<?php echo $noo;?>"> <?php echo $icon; ?> </a>
										<?php
                                        if($pop_vid || $pop_txt) {?>
											<div style="display:none; height:auto !important" id="contentid<?php echo $noo;?>" class="attachvideo">
                                				<?php if ($pop_txt){ echo "<p>$pop_txt</p>"; } ?>
                                				<?php if ($pop_vid) {?>
                               						<iframe src="<?php echo $pop_vid;?>" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												<?php } ?>
                                				<span class="signature"><?php echo $pop_tit;?></span>
                                            </div>
										<?php } ?>
									<?php } else { ?>
                                                                                
                                                                                
                                                                                
<?php //added to use different way to select a link
$posts = $btn['button_text'];

if( $posts ): ?>

    <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
        <?php setup_postdata($post); 

             $btnlinka = get_permalink();

    endforeach; ?>

    <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
<?php endif; 

if ($btn['outbound_link']) {
    if (($btn['the_outbound_link']) !='' ){
         $btnlinka = $btn['the_outbound_link'];
    }
}  
if ($btn['new_window']){
    $newwindow = '_blank';
} else {
    $newwindow = '';
}?>                                                                                
                                                                                
                                                                                
										<a class="button-link" title="<?php echo $btn['button_label']; ?>" href="<?php echo $btnlinka;//echo $btn['button_text']; ?>" target="<?php echo $newwindow;?>"> <?php echo $btn['button_label']; ?></a>                                        
									<?php } ?>
								<?php }
                        	} ?>
					</div>
                	<?php } ?>
				</div>
			</section>
    		<?php
	
			// SECTION end
	
			// SUB PAGES start
			if (get_sub_field('sub_pages_list')): ?>
			<section id="feature-section" class="<?php echo $css_class;?> pbt120 sub-pages" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
              <div class="container">
                  <?php $arraySize = sizeof(get_sub_field('sub_pages_list'));
                      if($arraySize < 4) { $add_css = 'no-margin-bottom'; } else { $add_css = ''; }
					  
					  if($arraySize == 1){$arraySize = 1;}
                      if($arraySize == 2){$arraySize = 2;}
                      if($arraySize == 3){$arraySize = 3;}
					  if($arraySize == 4){$arraySize = 2;}
					  if($arraySize > 4){$arraySize = 3;}
		
                  ?>
                  <ul class="<?php echo 'col-'.$arraySize;?>">
                      <?php
                      foreach(get_sub_field('sub_pages_list') as $subPages){
                      ?>
                      
                      <li class="<?php echo $add_css;?> childpage-resize-<?php echo $el;?>">
                          <figure>
                          <a href="<?php echo $subPages['sub_link']; ?>" title="<?php echo $subPages['sub_title']; ?>"><img src="<?php echo $subPages['sub_icon_image']['url']; ?>" alt="<?php echo $subPages['sub_link_title']; ?>" title="<?php echo $subPages['sub_title']; ?>"></a>
                          </figure>
                          <h4 class="mtb15"><a href="<?php echo $subPages['sub_link']; ?>" title="<?php echo $subPages['sub_link_title']; ?>"><?php echo $subPages['sub_title']; ?></a></h4>
                          <p><?php echo $subPages['sub_content']; ?></p>
                          <?php if ($subPages['sub_link_title']){?>
                          <a href="<?php echo $subPages['sub_link']; ?>" class="links" title="<?php echo $subPages['sub_link_title']; ?>"><?php echo $subPages['sub_link_title']; ?><span class="fa fa-angle-right"></span></a>
                          <?php } ?>
                      </li>
                      <?php }
                      ?>
                  </ul>
              </div>
			</section>
			<?php endif;
			// SUB PAGES End
			
			// TEXT LAYOUT end

//////////////////////////////////////////////////////////
// SLIDER
			
        elseif( get_row_layout() == 'slider' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 slider" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="flexslider">
                          <ul class="slides">
                          <?php foreach(get_sub_field('slider') as $sl){ ?>
                              <li>
                                  <img alt="<?php echo $sl['title']; ?>" title="<?php echo $sl['title']; ?>" src='<?php echo $sl['image']['url']; ?>' />
                                  <?php if ($sl['title'] || $sl['text']) { ?>
                                  <div class="cover-text <?php echo $sl['text_position']; ?>">
                                      <?php if ($sl['title']) { echo "<h3>".$sl['title']."</h3>"; } ?>
                                      <?php if ($sl['text']) { echo "<p>".$sl['text']."</p>"; } ?>
                                      <?php if ($sl['button_link'] && $sl['button_text']) { echo "<a href='".$sl['button_link']."'>".$sl['button_text']."</a>"; } ?>                
                                  </div>
                                  <?php } ?>
                              </li>
                          <?php } ?>
                      

                          </ul>
                      </div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// SLIDER LAYOUT ends


//////////////////////////////////////////////////////////
// TESTIMONIALS
			
        elseif( get_row_layout() == 'testimonials' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 testimonials" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="container" id="testimonial-sections">
						<?php
						$title = get_sub_field('title');
						$content = get_sub_field('content');
						$link = get_sub_field('link');
						$link_title = (get_sub_field('link_title'))? get_sub_field('link_title') : 'Read More';
						
                        $content_aligment = get_sub_field('content_aligment');
                        if($content_aligment == 'left') {
                            $image_aligment = 'right';
                        }
                        else {
                            $image_aligment ='left';
                        }
                        ?>
                        
                        <div class="col-half <?php echo $content_aligment; ?>">
                            <h2><?php echo $title; ?></h2>
                            <p><?php echo $content; ?></p>
                            <p><a href="<?php echo $link; ?>" class="button-link"><?php echo $link_title; ?></a></p>
                        </div>
                        
                        <div class="col-half <?php echo $image_aligment; ?>">
                       		
         					<div style="position:relative">
                            
                              <div class="slider-testimonial">
                                <ul class="slides">
                                <?php foreach(get_sub_field('items') as $item) { ?>
                               
                               <li>
                              	  <div class="t-content">
                                  	<?php $string = $item->post_content;
									$string = substr($string,0,175).'';
									echo '<p>'.strip_tags($string).'(...)</p>';?>
                                  </div>                    
                              		<figure>
                                    	<?php $img = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 't-home-image'); ?> 
                         				<img alt="<?php echo $item->post_title; ?>" class="attachment-t-home-image wp-post-image" src="<?php echo $img[0]; ?>">
                                        <h4><?php echo $item->post_title; ?></h4>
                              			<p class="main-para center"><?php the_field('author_post',$item->ID); ?></p>
                              		</figure>
                       	 		
                       
                          	  </li>
                              <?php } ?>       
                              </ul>
                            </div>

						</div>

                      </div>
					</div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// TESTIMONIAL LAYOUT ends
			
			
//////////////////////////////////////////////////////////
// CALL TO ACTION
			
        elseif( get_row_layout() == 'form' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			// bg image
			$background_image = get_sub_field('background_image');
			if ($background_image) {
				$css_bg = 'style="background:url('.$background_image['url'].') no-repeat;background-size:cover; background-position:center;"';
			}


			
			$fw = get_sub_field('full_width_slider');
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120" <?php echo $css_bg; ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      
						<?php
						if ($fw) { $add_padding = ' add_padding '; } else { $add_padding=''; }
						$title = get_sub_field('title');
						$content = get_sub_field('content');
						$form = get_sub_field('form');
						$link = get_sub_field('link');
						$link_title = (get_sub_field('link_title'))? get_sub_field('link_title') : 'Read More';
						
                        $content_aligment = get_sub_field('content_aligment');
                        if($content_aligment == 'left') {
                            $image_aligment = 'right';
                        }
                        else {
                            $image_aligment ='left';
                        }
                        ?>
                        
                        <div class="col-half <?php echo $add_padding.$content_aligment; ?>">
                            <?php if ($title) { echo '<h2>'.$title.'</h2>';} ?>
                            <p><?php echo $content; ?></p>
                        </div>
                        
                        <div class="col-half <?php echo $add_padding.$image_aligment; ?>">
                       		<p><?php echo $form; ?></p>
                      </div>
					    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// CALL TO ACTION LAYOUT ends			



//////////////////////////////////////////////////////////
// FAQ
			
        elseif( get_row_layout() == 'faq' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 testimonials" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="container" id="testimonial-sections">
						<?php
						$title = get_sub_field('title');
						$content = get_sub_field('content');
						$link = get_sub_field('link');
						$link_title = (get_sub_field('link_title'))? get_sub_field('link_title') : 'Read More';
                        $content_aligment = get_sub_field('content_aligment');
						switch ($content_aligment) {
							case 'left':
								$content_aligment = 'col-half left';
								$image_aligment ='col-half right';
								break;
							case 'right':
								$content_aligment = 'col-half right';
								$image_aligment ='col-half left';								
								break;
							case 'full width':
								$content_aligment = 'col-full';
								$image_aligment ='col-full';								
								break;
							default:
								$content_aligment = 'col-full';
								$image_aligment ='col-full';							
								break;
						}
                        ?>
                        
                        <div class="<?php echo $content_aligment; ?>">
                            <h2><?php echo $title; ?></h2>
                            <p><?php echo $content; ?></p>
                        </div>
                        
                        <div class="<?php echo $image_aligment; ?>">
                       		
         					<div class="faqs">
 								<ul>
    								<?php
									$no=0;
									foreach(get_sub_field('items') as $faq) {
                                    	$no++;
										echo "<li rel='faq-".$no."'>";
                                    	echo "<h3>".$faq['question']."<span>+</span></h3>";
                                        echo "<div><p>".$faq['answer']."</p></div></li>";
									}
                                   ?>
                             	</ul>
                           	</div>
                      </div>
					</div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// FAQ LAYOUT ends



///////////////////////////////////////////////////////////
// BOXES			
			
			elseif( get_row_layout() == 'boxes' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			$item_per_row = (get_sub_field('elements'))? "c-".get_sub_field('elements') : "";
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 boxes" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="">
                          <ul class="rboxes rboxes-<?php echo $el;?> <?php echo $item_per_row;?>">
						  <?php 
                          $i=1;
                          foreach(get_sub_field('boxes') as $boxes) {
                          $box_image =  $boxes['image']['sizes']['page-box'];
                          $box_title = $boxes['title'];
                          $box_link = $boxes['button_link'];
						  $box_link_dis = $boxes['display_link'];
                          $box_link_txt = ($boxes['button_text'])? $boxes['button_text'] : "Read More";
                          $box_content = $boxes['text'];
                          
                          
                          
                          
                            //added to use different way to select a link
                            $posts = $boxes['button_link'];

                            if( $posts ): ?>

                                <?php foreach( $posts as $post): // variable must be called $post (IMPORTANT) ?>
                                    <?php setup_postdata($post); 

                                         $box_link = get_permalink();

                                endforeach; ?>

                                <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                            <?php endif; 

                            if ($boxes['outbound_link']) {
                                if (($boxes['the_outbound_link']) !='' ){
                                     $box_link = $boxes['the_outbound_link'];
                                }
                            }  
                            if ($boxes['new_window']){
                                $newwindow = '_blank';
                            } else {
                                $newwindow = '';
                            }
                            ////////////////////////////////////////////      
                          
                          
                          //$box_over_bg = $boxes['link_color'];
						  //$box_over_bg_opacity = hex2rgba($box_over_bg, 1); // background with opacity
                          //$box_over_bg_opacity2 = hex2rgba($box_over_bg, 0.2); // background with opacity
              				
		
                          if($i%(get_sub_field('elements'))==0){$class="even last-box";} else{ $class ="even";}
                          
                          echo '<li class="'.$class.'">';
                          ?> 	
                              <div>
                                  <?php if ($box_link_dis) { ?>
                                  	
                                  	<?php if ($box_image) { ?>
                                  	<a class="" href="<?php echo $box_link; ?>" target="<?php echo $newwindow; ?>"><img class="box-image" alt="" src="<?php echo $box_image; ?>" /></a>
                                  	<?php } ?>
                                  	<h3 class="box-title"><a href="<?php echo $box_link; ?>"><?php echo $box_title; ?></a></h3>
                                    <p><?php echo $box_content; ?></p>
                                    </div>
                              		<p class="btn-bottom"><a class="large-color-button" target="<?php echo $newwindow; ?>" href="<?php echo $box_link; ?>"><?php echo $box_link_txt; ?></a></p>
                                  <?php } else { ?>
                                  	
                                  	<?php if ($box_image) { ?>
                                  	<img class="box-image" alt="" src="<?php echo $box_image; ?>" />
                                  	<?php } ?>
                                  	<h3 class="box-title"><?php echo $box_title; ?></h3>
                                    <p><?php echo $box_content; ?></p>
                                    </div>                                
                                  <?php } ?>

                              <div class="clear"></div>
                          
                          <?php
                          echo '</li>';
                          $i++;
                          } ?>
                         </ul>
                      </div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// BOXES LAYOUT ends
	


///////////////////////////////////////////////////////////
// CHILDPAGES			
			
			elseif( get_row_layout() == 'childpages' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			$item_per_row = (get_sub_field('elements'))? "c-".get_sub_field('elements') : "";
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 boxes" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="">
                         
                        <?php
                        $childpages = query_posts('post_per_page=20&orderby=menu_order&order=asc&post_type=page&post_parent='.$thispage);
                        if($childpages){
						?>
                    	<ul class="rboxes rboxes-<?php echo $el; ?> <?php echo $item_per_row;?>">
							<?php 
                          	$i=1;
                          	foreach ($childpages as $post) {
								setup_postdata($post);
                          		if($i%(get_sub_field('elements'))==0){$class="even last-box";} else{ $class ="even";}
                         		echo '<li class="'.$class.'">';?> 	
                          		<div>
                                	
									<?php
									$image_ch = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
									if ($image_ch) { ?>
                                  	<a class="" href="<?php the_permalink(); ?>"><img alt="<?php the_title(); ?>" src="<?php echo $image_ch[0];?>" /></a>
                                  	<?php } ?>
									<h3 class="box-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php
									$ex = get_field('excerpt', $post->ID);
									$link_title = get_field('link_title', $post->ID)? get_field('link_title', $post->ID) : 'Read More';
									if ($ex) {
										$ex = $ex;
									} else {
										 $ex = substr(get_the_content(), 0,200);
									}
									$html = strip_tags($ex);
									echo '<p>'.$html.'</p>'; 
									?>
                              	</div>
                              	<p class="btn-bottom"><a class="large-color-button" href="<?php the_permalink(); ?>"><?php echo $link_title; ?></a></p>
                           		<div class="clear"></div>
                          		<?php
                          	echo '</li>';
                          	$i++;
                         } ?>
                       </ul>
                       <?php
                       }
                       wp_reset_query();
                       ?>
                         
                      </div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// CHILDPAGES LAYOUT ends
	


///////////////////////////////////////////////////////////
// RELATED PAGES			
			
			elseif( get_row_layout() == 'related' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
			
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width_slider');
			$item_per_row = (get_sub_field('elements'))? "c-".get_sub_field('elements') : "";
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 boxes" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="">
                         
                        <?php
                        $childpages = get_sub_field('select_pages');
						if($childpages){
						
						?>
                    	<ul class="rboxes rboxes-<?php echo $el; ?> <?php echo $item_per_row;?>">
							<?php 
                          	$i=1;
                          	foreach ($childpages as $post) {
								setup_postdata($post);
                          		if($i%(get_sub_field('elements'))==0){$class="even last-box";} else{ $class ="even";}
                         		echo '<li class="'.$class.'">';?> 	
                          		<div>
                                	
									<?php
									$image_ch = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
									if ($image_ch) { ?>
                                  	<a class="" href="<?php the_permalink(); ?>"><img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $image_ch[0];?>" /></a>
                                  	<?php } ?>
									<h3 class="box-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
									<?php
                                    $ex = get_field('excerpt', $post->ID);
									$link_title = get_field('link_title', $post->ID)? get_field('link_title', $post->ID) : 'Read More';
									if ($ex) {
										$ex = $ex;
									} else {
										 $ex = substr(get_the_content(), 0,200);
									}
									$readmore = "...";
									$html = strip_tags($ex);
									echo '<p>'.$html.$readmore.'</p>';  
									?>
                              	</div>
                              	<p class="btn-bottom"><a class="large-color-button" href="<?php the_permalink(); ?>"><?php echo $link_title; ?></a></p>
                           		<div class="clear"></div>
                          		<?php
                          	echo '</li>';
                          	$i++;
                         } ?>
                       </ul>
                       <?php
                       }
                       wp_reset_query();
                       ?>
                         
                      </div>    
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// RELATED PAGES ends
	
	

			
///////////////////////////////////////////////////////////
// LOGOS			
			
			elseif( get_row_layout() == 'logos' ): 

			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
						
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			
			$fw = get_sub_field('full_width');
			//$item_per_row = (get_sub_field('elements'))? "c-".get_sub_field('elements') : "";
			?>
            
              <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> pbt120 boxes" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
                  <?php if(!$fw) { echo '<div class="container">'; } ?>
                      <div class="">
            
                        <?php
						$clients = get_sub_field('elements');
						if($clients) { 
							echo '<div id="owl-logos" class="owl-carousel">';
							foreach($clients as $logo){
								  $t = $logo['title'];
								  $img = $logo['image']['sizes']['clients-small'];
								
								  $link = $logo['add_link'];
								  $link_t = ($logo['link_title'])? $logo['link_title']: "Read More ";
								  $add_link = 'false';
								  if ($link == 'internal') {
									  $add_link = 'true';
									  $where = $logo['link_internal'];
									  $target = "";
								  }
								  else if ($link == 'external') {
									  $add_link = 'true';
									  $where = $logo['link_external'];
									  $target = "target='_blank'";	
								  }
								  else {
									  $add_link = 'false';	
								  }
								
								echo '<div class="item">';
									  if ($img) {
										  // if image	
										  // if link 
										  if ($add_link=='true') { echo "<a title='".$t."' href='".$where."' ".$target.">"; }
										  echo "<img src='".$img."' alt='".$t."' title='".$t."'/>";
										  
										 
										 
										 if ($add_link=='true') { echo "</a>"; }
									  }
									  
								echo '</div>';
								
							}
							echo '</ul></div>';
						} ?>
                        
                  <?php if(!$fw) { echo '</div>'; } ?>
              </section>
        
			<?php
			// LOGOS LAYOUT ends
			
///////////////////////////////////////////////////////////
// VIDEO			
			
		elseif( get_row_layout() == 'video' ): 
			
			// CSS Class
			$css_class = "".get_sub_field('css_class')." ";
						
			$scroll_id = 'item-'.$el;
			$el++;
		
			$css_bg = 'style="background:#ffffff"';
			// bg color
			$background_color = get_sub_field('background_color');
			if($background_color) {
				$css_bg = 'style="background:'.$background_color.'"';
			}
			// bg image
			$background_image = get_sub_field('background_image');
			if ($background_image) {
				$css_bg = 'style="background:url('.$background_image['url'].') no-repeat;background-size:cover; background-position:center;"';
			}
			// bg video
			$video_file = get_sub_field('background_video');
			$background_video_mime = ($video_file['mime_type'])? $video_file['mime_type'] : "video/mp4";
			$background_video_url = $video_file['url'];
					
			// if bg image set up color for text
			$css_txt = "";
			$background_color_content = get_sub_field('background_color_content');
			if ($background_color_content) {
				$bg_for_text = hex2rgba($background_color_content, 0.7);
			} else {
				$bg_for_text = 'none';// hex2rgba('#ffffff', 0.7);
			}
			
			// if bg video set up width for text
			if($background_video_url) {
				$css_txt = 'style="background:'.$bg_for_text.'; width:31%; padding:2%"';
			}
			
			// image / text aligment 
			$content_aligment = get_sub_field('content_aligment');
			
			
			
			// SECTION start
			
			$fwv = get_sub_field('full_width_video'); ?>
            <section id="<?php echo $scroll_id; ?>" class="<?php echo $css_class;?> ptb120 video">
        		<div class="<?php if (!$fwv) { echo 'container';} ?>" style="position:relative">

                	<div class="bg-video" style="position:absolute;overflow:hidden;width:100%;height:100%;z-index:1" >
                		  <video class="thevideo" poster="<?php echo $background_image['url']; ?>" loop="" autoplay="">
							<source type="<?php echo $background_video_mime; ?>" src="<?php echo $background_video_url; ?>"></source>
				 </video>
                	</div>
                	<div class="container" style="position:relative;overflow:hidden;width:100%;height:100%;z-index:2">
                    	<div class="col-half <?php echo $content_aligment; ?>" <?php echo $css_txt; ?>>
							<?php if(!get_sub_field('hide_title')) { echo "<h2>".get_sub_field('title')."</h2>"; } ?>
							<?php echo '<p>'.get_sub_field('content').'</p>'; ?>
                            
                            <?php 
                            $buttons  = get_sub_field('buttons');
                            if($buttons) { 
                            	foreach($buttons as $btn){
									if ($btn['button_type'] != 'regular') { ?>
                                       	<?php
                                    	$icon = ($btn['button_type'] == 'play') ? '<span class="fa fa-caret-right"></span>' : '<span class="fa fa-quote-right"></span>'; 
										$pop_txt = $btn['pop-up_text'];
										$pop_vid = $btn['pop-up_video'];
										$pop_tit = ($btn['pop-up_title'])? $btn['pop-up_title'] : $btn['button_label'];
										$noo++;
										?>
										<a class="fancybox button-link play <?php echo $btn['button_type'];?>" title="<?php echo $pop_tit; ?>" href="#contentid<?php echo $noo;?>"> <?php echo $icon; ?> </a>
										<?php
                                        if($pop_vid || $pop_txt) {?>
											<div style="display:none; height:auto !important" id="contentid<?php echo $noo;?>" class="attachvideo">
                                				<?php if ($pop_txt){ echo "<p>$pop_txt</p>"; } ?>
                                				<?php if ($pop_vid) {?>
                               						<iframe src="<?php echo $pop_vid;?>" width="560" height="315" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
												<?php } ?>
                                				<span class="signature"><?php echo $pop_tit;?></span>
                                            </div>
										<?php } ?>
									<?php } else { ?>
										<a class="button-link" title="<?php echo $btn['button_label']; ?>" href="<?php echo $btn['button_text']; ?>"> <?php echo $btn['button_label']; ?></a>                                        
									<?php } ?>
								<?php }
                        	} ?>
                        </div>
                	</div>
                </div>
        	</section>
			
            <?php
			// SECTION end

			// VIDEO LAYOUT ends
			////////////////////////////////////////////////////////////
			
		endif;
		

    endwhile;

else :

    // no layouts found

endif;

?>


		

		

		<?php if ($display_clients) { ?>
		<!-- Clients section start -->
		<section id="clients-section" class="ptb120" style="background:#ffffff">
			<div class="container">
				<?php query_posts('post_type=page&p=42'); 
				while(have_posts()) : the_post();
				$image_aligment = get_field('image_alignment');
				if($image_aligment == 'left') {
					$content_aligment = 'right';
				} else {
					$content_aligment ='left';
				}
				?>
				<div class="col-half <?php echo $content_aligment; ?>">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<a href="<?php the_permalink(); ?>" class="button-link">Read more about Our Clients</a>
				</div>
				<div class="col-half <?php echo $image_aligment; ?>">
                <?php
                	
					
					// set up limits
                    $columns = 1;
                    $rows = 9;
                    $beforeStart[] = '';
    
					$clients = get_field('logos');
					if($clients) { 
						$imagesFromCat = '';
						foreach($clients as $logo){
							$t = $logo['title'];
							$l = $logo['link'];
							$img = $logo['image']['sizes']['clients-small'];
							$imagesFromCat .= $img.',';
						}
						$imagesFromCat = substr ( $imagesFromCat , 0, strlen($imagesFromCat)-1 );
						$beforeStart[0] = $imagesFromCat;
					}
           			?>
					
                    	<ul class="c-logos brand_slide" id="c-<?php echo($x+1);?>">
                        	<div class="array" style='display:none'><?php echo $beforeStart[0];?></div>
                            <?php
                            for($y=0; $y<$rows; $y++) { ?>
                            	<li class="brand_slides" id="r-<?php echo($x+1)?>-<?php echo($y+1);?>"></li>
                            <?php } ?>
                    	</ul>
         			

				</div>
				
				<?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>
				
			</div>
		</section>
        <!-- Clients section end
		<?php } ?>
        



<?php endwhile;  ?>
<?php get_footer(); ?>