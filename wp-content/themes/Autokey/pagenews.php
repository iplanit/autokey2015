<?php
get_header(); 
wp_reset_query();
while( have_posts() ) : the_post();
$thispage=$post->ID;
?>
	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section" class="fixed" style="margin-top:0px"></section>	
	<?php } ?>
	<!-- top section end -->
		

	<?php
    // additional options
    $display_content = get_field('display_main_content' ,$post->ID);
	$display_clients = get_field('display_clients' ,$post->ID);
	$display_contact_link = get_field('display_contact_link' ,$post->ID); 
	?>
	
   
    	
    <!-- page content -->
   <?php
require('/the/path/to/your/wp-blog-header.php');
?>

<?php query_posts('showposts=3'); ?>
<?php while (have_posts()) : the_post(); ?>
" rel="bookmark" title="Permanent Link to <?php the_title(); ?>"><?php the_title(); ?>
<?php endwhile;?>


		

		

		<?php if ($display_clients) { ?>
		<!-- Clients section start -->
		<section id="clients-section" class="ptb120" style="background:#ffffff">
			<div class="container">
				<?php query_posts('post_type=page&p=42'); 
				while(have_posts()) : the_post();
				$image_aligment = get_field('image_alignment');
				if($image_aligment == 'left') {
					$content_aligment = 'right';
				} else {
					$content_aligment ='left';
				}
				?>
				<div class="col-half <?php echo $content_aligment; ?>">
					<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
					<a href="<?php the_permalink(); ?>" class="button-link">Read more about Our Clients</a>
				</div>
				<div class="col-half <?php echo $image_aligment; ?>">
                <?php
                	
					
					// set up limits
                    $columns = 1;
                    $rows = 9;
                    $beforeStart[] = '';
    
					$clients = get_field('logos');
					if($clients) { 
						$imagesFromCat = '';
						foreach($clients as $logo){
							$t = $logo['title'];
							$l = $logo['link'];
							$img = $logo['image']['sizes']['clients-small'];
							$imagesFromCat .= $img.',';
						}
						$imagesFromCat = substr ( $imagesFromCat , 0, strlen($imagesFromCat)-1 );
						$beforeStart[0] = $imagesFromCat;
					}
           			?>
					
                    	<ul class="c-logos brand_slide" id="c-<?php echo($x+1);?>">
                        	<div class="array" style='display:none'><?php echo $beforeStart[0];?></div>
                            <?php
                            for($y=0; $y<$rows; $y++) { ?>
                            	<li class="brand_slides" id="r-<?php echo($x+1)?>-<?php echo($y+1);?>"></li>
                            <?php } ?>
                    	</ul>
         			

				</div>
				
				<?php endwhile; wp_reset_postdata(); wp_reset_query(); ?>
				
			</div>
		</section>
        <!-- Clients section end
		<?php } ?>
        



<?php endwhile;  ?>
<?php get_footer(); ?>