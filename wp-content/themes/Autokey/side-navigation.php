<section id="side-navigation">
            <ul class="tabs-navigation">
            	                  <?php if ($display_content) {?>
				<li><a class='scroll-down' href='#content'><span class="show-on-hover"><?php the_title(); ?></span><i>&nbsp;</i></a></li>
                <?php } ?>
				
				<?php if( have_rows('section') ):
					$el = 0;
					// loop through the rows of data
					while ( have_rows('section') ) : the_row();
						
						if( get_row_layout() == 'text' ):
			  				
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif; 
						
						elseif( get_row_layout() == 'slider' ): 
							
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;			  
						
						elseif( get_row_layout() == 'video' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
												  	
						
						elseif( get_row_layout() == 'boxes' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
							
						elseif( get_row_layout() == 'logos' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
							
						elseif( get_row_layout() == 'testimonials' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
							
						elseif( get_row_layout() == 'faq' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
							
						elseif( get_row_layout() == 'childpages' ): 
			  
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;	
							
						elseif( get_row_layout() == 'form' ): 
						
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;
							
						elseif( get_row_layout() == 'related' ): 
						
							$scroll_id = 'item-'.$el;
							$el++;
							$titledisplay = (get_sub_field('menu_title'))? get_sub_field('menu_title') : get_sub_field('title');
							$add2nav = get_sub_field('attach_to_top_nav');
							if ($add2nav) : ?>
								<li><a class="scroll-down" href="<?php echo '#'.$scroll_id; ?>"><span class="show-on-hover"><?php echo $titledisplay; ?></span><i>&nbsp;</i></a></li>
			  				<?php endif;																					
																					
												  	
						endif;
							
				  	endwhile;
				endif; ?>
                
				<?php
                if ($display_clients) {
                	echo '<li><a class="scroll-down" href="#clients-section"><span class="show-on-hover">Clients</span><i>&nbsp;</i></a></li>';
				}
				
				 if ($display_contact_link) {
                	echo '<li><a class="scroll-down" href="#map"><span class="show-on-hover">Find Us</span><i>&nbsp;</i></a></li>';
				}
				?>
                    </ul>
	</section>