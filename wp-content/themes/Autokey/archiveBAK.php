<?php get_header(); ?>

<!-- navigation start -->
		<section id="navigation">
			<div class="container">

            <ul class="tabs-navigation left">
				
                <li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                
            </ul>
            
            <ul class="tabs-navigation right">
                <li><a class="scroll-down" href="#navigation"><?php
					if ( is_day() ) :
						printf( __( 'Daily Archives: %s', 'twentytwelve' ), '<span>' . get_the_date() . '</span>' );
					elseif ( is_month() ) :
						printf( __( 'Monthly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'F Y', 'monthly archives date format', 'twentytwelve' ) ) . '</span>' );
					elseif ( is_year() ) :
						printf( __( 'Yearly Archives: %s', 'twentytwelve' ), '<span>' . get_the_date( _x( 'Y', 'yearly archives date format', 'twentytwelve' ) ) . '</span>' );
					else :
						_e( 'Archives', 'twentytwelve' );
					endif;
				?></a></li>
                <li><a class="scroll-down" href="#map">Contact</a></li>
			</ul>
            
			<?php
			/*
			<a class="toggle-menu" href="javascript: void(0);"><small>Menu</small> <span></span></a>
			*/
			?>
            </div>
		</section>
		<!-- navigation end -->





		 <div class="container">

			<main role="main">
				<!-- section -->
			
			<?php
		while(have_posts()) : the_post();
			    $image_aligment = get_field('image_alignment', $post->ID);
				if($image_aligment == 'left')
				{
				$content_aligment = 'right';
				}
				else
				{
				$content_aligment ='left';
				}
			?>	
		      <section  class="ptb120">
               <div class="container">
				<div class="col-half <?php echo $image_aligment; ?>">
				<figure class="<?php echo 'align_'.$image_aligment; ?>">
				<?php the_post_thumbnail(); ?>
				</figure>
				</div>
				<div class="col-half <?php echo $content_aligment; ?>">
					<h2><?php the_title(); ?></h2>
					<?php $string = get_the_content($post->ID);
					$string = substr($string,0,275).'';
					echo '<p>'.strip_tags($string).'</p>';
					?>
					<a href="<?php the_permalink(); ?>" class="button-link mright">learn more</a>
				</div>
			</div>
		</section>
			
		<?php
		endwhile; wp_reset_query();
		?>

					<?php get_template_part('pagination'); ?>

				<!-- /section -->
			</main>
</div>
<?php //get_sidebar(); ?>

<?php get_footer(); ?>
