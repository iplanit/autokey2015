<?php
get_header(); 
?>

	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section"></section>	
	<?php } ?>
	<!-- top section end -->
		

		
	<!-- navigation start -->
	<section id="navigation">
		<div class="container">
            <ul class="tabs-navigation left">
				<li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
         	</ul>
            
            <ul class="tabs-navigation right">

				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
			</ul>
        
       	</div>
	</section>
    <div class="sub-navigation">
    	<div class="container">
    		<div class="left">
            	<div class="breadcrumb">
					You are here:
						<a href="<?php echo home_url(); ?>">Home</a> » <?php echo sprintf( __( 'Results for: ', 'html5blank' )); echo '<span>'.get_search_query().'</span>'; ?>
</div></div>
            <div class="right"><?php dynamic_sidebar('phone-number'); ?></div>
        </div>
        <a href="#" class="bttn hide" title="Close">X</a>
        <a href="#" class="bttn show" title="Show">\/</a>
    </div>
    
	<!-- navigation end -->
	
    <div class="empty-space"></div>
    	

 
      <section id="content" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
          <div class="container">
              <div class="col-content">
  				<div class="left col-half">
             		<h2 class='p-title'><?php echo sprintf( __( '%s results for: ', 'html5blank' ), $wp_query->found_posts ); echo '<span>'.get_search_query().'</span>'; ?></h2>
				</div>
                <div class="right col-half align-right">
                 <p>Didn't find what you are looking for? Try again.</p>
                  <!-- search -->
                  <form class="search-page" method="get" action="<?php echo home_url(); ?>" role="search">
                      <button class="search-submit" type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
                      <input class="search-input" type="search" name="s" placeholder="<?php _e( 'Search for your make and model now', 'html5blank' ); ?>">
                      
                  </form>
                  <!-- /search -->

				</div>
				<div class="clearfix"></div>
				
				
				<?php get_template_part('pagination'); ?>
                
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
					<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
						<h2><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h2>
						<?php html5wp_excerpt('html5wp_index'); // Build your custom callback length in functions.php ?>
					</article>

				<?php endwhile; ?>
                
                <?php else: ?>
                    <article>
                        <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                    </article>
                <?php endif; ?>

				<?php get_template_part('pagination'); ?>
              </div>
          </div>
      </section>
	

<?php get_footer(); ?>
