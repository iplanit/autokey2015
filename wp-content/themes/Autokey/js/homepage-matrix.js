jQuery(function($) {

	// parameters
	var columns = 1;
	var rows = minImages = 9;
	
	var mainArray = [];
	var arrayLength = [];
	var arrayToDisplay = [];
	var areUndisplayed = [];
	var positions = [];
	var imgToChange = [];
	
	// functions
	function shuffleArray(array) {
	  for (var i = array.length - 1; i > 0; i--) {
		  var j = Math.floor(Math.random() * (i + 1));
		  var temp = array[i];
		  array[i] = array[j];
		  array[j] = temp;
	  }
	  return array;
	}
		
	function addItemsToArray(thisArray){
		var howManyItems = (minImages+1) - thisArray.length;
		var i=0;
		var from = Number(thisArray.length);
		var to = Number(thisArray.length+howManyItems);
		for (var j=from; j<to; j++) {
			thisArray.push(thisArray[i]);
			i++;		
		}
	}
	
	function findNextToDisplay() {
		var selectedColumn = Math.floor((Math.random() * columns) + 0);
		var selectedRow = Math.floor((Math.random() * rows) + 0);
		var changeThis = arrayToDisplay[selectedColumn][selectedRow];
		var selectedRowNew = Math.floor((Math.random() * areUndisplayed[selectedColumn].length) + 0);
		var newImage = areUndisplayed[selectedColumn][selectedRowNew];
		
		var thisTime = Math.floor((Math.random() * 3000)); 
		if (thisTime < 150 ) { thisTime = 1000; }
		
		thisTime = 1500;
		
		arrayToDisplay[selectedColumn][selectedRow] = newImage;
		areUndisplayed[selectedColumn][selectedRowNew] = changeThis;
		
		var updateThisBlock = "#r-"+(selectedColumn+1)+"-"+(selectedRow+1)+"";
		loadInsertImage(selectedColumn,selectedRow,updateThisBlock,newImage);
		
		window.setTimeout(findNextToDisplay,thisTime);
	}
	
	function loadInsertImage(i,j,thisElem, thisImgUrl) {
		var newElem = '<img style="" src="'+thisImgUrl+'" alt="" />';
		var imgElem = ''+thisElem+' img';
		var thisTime = Math.floor((Math.random() * 1000));
		if (thisTime < 700 ) { thisTime = 700; }
		$(thisElem).animate({opacity: 0}, (thisTime/2), function() {
			$(thisElem).html(newElem);
			$(thisElem).animate({opacity:1},thisTime);
		});
	}
	
	
	
	// steps
	if(document.getElementById("clients-section") !== null){
	
		// 1
		for (var i=0; i<columns; i++) {
			mainArray[i] = ($('#c-'+(i+1)+' .array').html()).split(",");	
		}
		
		// 2 
		for (var i=0; i<columns; i++) {	
			if (mainArray[i].length < (minImages+1)) {
				addItemsToArray(mainArray[i]);	
			}
		}
		
		// 3
		for (var i=0; i<columns; i++) {
			mainArray[i] = shuffleArray(mainArray[i]);	
		}
		
		// 4 create array to display and array to change
		for (var i=0; i<columns; i++) {
			var tmp1 = [];
			var tmp2 = [];
			for (var j=0; j<Number(mainArray[i].length); j++) {
				if (j<rows) {
					tmp1.push(mainArray[i][j]);	
				} else {
					tmp2.push(mainArray[i][j]);
				}
			}
			arrayToDisplay[i] = tmp1;
			areUndisplayed[i] = tmp2;
		}
		
		// 5 check new arrays
		/*
		for (var i=0; i<columns; i++) {	
			console.log('array to display '+i+' : '+arrayToDisplay[i]);
			console.log('array to swap '+i+' : '+areUndisplayed[i]);
		}
		*/
		
		// 6 set up first displey for all elements
		for (var i=0; i<columns; i++) {
			for (var j=0; j<rows; j++) {
				var updateThisBlock = "#r-"+(i+1)+"-"+(j+1)+"";
				loadInsertImage(i,j,updateThisBlock,arrayToDisplay[i][j]);
			}
		}
		
		// 7 find next image to display
		window.setTimeout(findNextToDisplay,3000);
		
		}

	
	
});