$(document).ready(function(){
	
	
	$('.scroll-down').on('click',function(e){
		e.preventDefault();
		
		if($('#navigation').hasClass('fixed-navigation')){
			$('html, body').animate({
				'scrollTop': $($(this).attr('href')).position().top-80-$('.sub-navigation').height()+2
			},1000);
		}else{
			$('html, body').animate({
				'scrollTop': $($(this).attr('href')).position().top-($('#navigation').outerHeight()+2)-$('.sub-navigation').height()
			},1000);
		}
		
	});
	
	$('.toggle-menu').on('click',function(e){
		e.preventDefault();
		
		var marginwidth = $('nav').width();
		$('nav').css('right', '0');
		$('section').css('margin-left', '-' + marginwidth+'px');
		$('footer').css('margin-left', '-' + marginwidth+'px');
	});
	$('.nav-close').on('click',function(e){
		e.preventDefault();
		$('nav').removeAttr("style");
		$('section').css('margin-left', '0%');
		$('footer').css('margin-left', '0px');
	});
	
	// set up initial positions
	var nav_margin = $('#top-section').height()+'px';
	if (nav_margin > '0px') {
		$('#navigation').css('margin-top', nav_margin );
		$('.sub-navigation').css({"display": "none"});	
	} else {
		$('#navigation').css('margin-top', '0px' );
		$('.toggle-menu.top-toggle').addClass("fixed");
		$('#navigation').css('margin-bottom', '35px' );
	}
	
		
	// HIDE / SHOW sub nav
	$(".sub-navigation .hide").click(function(e){
		e.preventDefault();
		$( ".sub-navigation" ).animate({top: "35"}, 500, function() {
			// Animation complete.
			$('.sub-navigation').css('height', '1px' );
			$( ".sub-navigation .show" ).animate({top: "48"}, 700, function() {
				// Animation complete.
			});
		});
		$( "#navigation" ).animate({marginBottom: "0"}, 300, function() {
			// Animation complete.
		});
		
		//check if has fixed nav and do animation here ...
		if($('#navigation').hasClass('fixed-navigation')){
			$( ".empty-space" ).animate({height: "80"}, 300, function() {
				// Animation complete.
			});
		}
		
	});
	
	$(".sub-navigation .show").click(function(e){
		e.preventDefault();
		$( ".sub-navigation .show" ).animate({top: "-30"}, 700, function() {
			// Animation complete.
			$('.sub-navigation').css('height', '40px' );
			$( ".sub-navigation" ).animate({top: "80"}, 500, function() {
				// Animation complete.
			});
			$( "#navigation" ).animate({marginBottom: "35"}, 800, function() {
				// Animation complete.
			});
			
			//check if has fixed nav and do animation here ...
			if($('#navigation').hasClass('fixed-navigation')){
				$( ".empty-space" ).animate({height: "120"}, 700, function() {
					// Animation complete.
				});
			}

		});
	});
	
	
	
	// set up FAQ if any
	$(".faqs > ul > li").addClass("no-active");	
	$(".faqs > ul > li").click(function(){
		if($(this).hasClass("no-active")){
			$(this).find("div").slideDown();
			$(this).removeClass("no-active");
			$(this).find("h3 span").html("-");
		}
		else {
			$(this).find("div").slideUp();
			$(this).addClass("no-active");
			$(this).find("h3 span").html("+");
		}
	});	
	
	
	
	// set up logo carousel
	var owl = $("#owl-logos");

    owl.owlCarousel({ 
		itemsCustom : [
			[0, 3],
			[450, 4],
			[800, 6],
			[1000, 8],
			[1200, 10],
			[1600, 12],
			[1920, 12]
		],
		navigation : false

    });
	
	
	
	
	$(window).scroll(function(){
		var windowScroll = $(window).scrollTop();
		var top_sec_height = $('#top-section').height();
		var para ='-'+((windowScroll)/2)+'px';
		
		if($('#navigation').hasClass('fixed-navigation')){
			//$('.sub-navigation').show(300);
			$('.sub-navigation').css({"display": "block"});
			//$( ".sub-navigation" ).animate({top: "80"}, 100, function() {	});
		}else{
			//$('.sub-navigation').hide(300);
			$('.sub-navigation').css({"display": "none"});
			//$( ".sub-navigation" ).animate({top: "-30"}, 10, function() {	});
		}
		
		if (windowScroll > top_sec_height) {
			$('#navigation').removeAttr('style');
			$('#navigation').css({"position": "fixed","left": 0,"top": 0});
			$('#navigation').addClass("fixed-navigation");
			$('#top-section').removeClass('fixed');
			$('#top-section').css({"top": 0});
			$('a.toggle-menu').addClass('fixed');
			$('.empty-space').css({"width": "100%","height": "80px"});
		}
		else if (windowScroll < top_sec_height){
			$('#navigation').removeAttr('style');
			$('#navigation').removeAttr("class");
			$('#top-section').addClass('fixed');
			$('#top-section').css({"top": para});
			var nav_margin = $('#top-section').height()+'px';
			$('#navigation').css('margin-top', nav_margin );
			$('a.toggle-menu').removeClass('fixed');
			$('.empty-space').css({"width": "100%","height": "0px"});
			
		};
		
	});
	
	$(document).keyup(function(e){
		
		if(e.keyCode === 27) {
			$('a.close').trigger('click');
			$('#nav-close').trigger('click');
	
		}
	});
		
});

$(document).ready(function(){
	
	if ($('ul li.tpl').length > 0) { 
		$('ul li.tpl').matchMaxHeight();
	}
	
	if ($('.childpage-resize').length > 0) { 
		$('.childpage-resize').matchMaxHeight();
	}
		
	for(var i=0; i<10; i++) {
		var cur = '.childpage-resize-'+i+'';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.rboxes-'+i+' li';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	
	if ($('.flexslider .slides li').length > 0) { 
		$('.flexslider').flexslider({
			animation: "slide"
	  	});
	}
	
	if ($('.slider-testimonial .slides li').length > 0) { 
		$('.slider-testimonial').flexslider({
			animation: "fade",
			controlNav: true,
			directionNav: false
	  	});
	}
	
});



$(window).load(function() {
	if ($('ul li.tpl').length > 0) { 
		$('ul li.tpl').matchMaxHeight();
	}
	
	if ($('.childpage-resize').length > 0) { 
		$('.childpage-resize').matchMaxHeight();
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.childpage-resize-'+i+'';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.rboxes-'+i+' li';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}

});



$(window).resize(function(){

	if ( $( '#navigation' ).hasClass( "fixed-navigation" ) ) {
		// is already fixed 
	} else {
		// not fixed
		var nav_margin = $('#top-section').height()+'px';
		$('#navigation').css('margin-top', nav_margin );
	}
	
	if ($('ul li.tpl').length > 0) { 
		$('ul li.tpl').matchMaxHeight();
	}
	
	if ($('.childpage-resize').length > 0) { 
		$('.childpage-resize').matchMaxHeight();
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.childpage-resize-'+i+'';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.rboxes-'+i+' li';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}

});


//plugin to match all heights equal to max height in selection
(function ($) {
	$.fn.matchMaxHeight = function () { 
		var items = $(this);
		$(items).attr('style', ''); 
		$(items).css({});
		var max = 0;
		for(var i=0; i<items.length ; i++){
			max = max < $(items[i]).height() ? 
			$(items[i]).height() : max;
		}
		$(items).css({'display': 'block', 'height': ''+max+'px'});
	}
})(jQuery);