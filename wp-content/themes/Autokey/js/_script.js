$(document).ready(function(){
	
	
	$('.scroll-down').on('click',function(e){
		e.preventDefault();
		
		if($('#navigation').hasClass('fixed-navigation')){
			$('html, body').animate({
				'scrollTop': $($(this).attr('href')).position().top-80
			},1000);
		}else{
			$('html, body').animate({
				'scrollTop': $($(this).attr('href')).position().top-($('#navigation').outerHeight()-2)
			},1000);
		}
		
	});
	
	$('.toggle-menu').on('click',function(e){
		e.preventDefault();
		
		var marginwidth = $('nav').width();
		$('nav').css('right', '0');
		$('section').css('margin-left', '-' + marginwidth+'px');
		$('footer').css('margin-left', '-' + marginwidth+'px');
		//$('#top-section .cover-text').css('margin-right', marginwidth + 'px');
		
	});
	$('.nav-close').on('click',function(e){
		e.preventDefault();
		$('nav').removeAttr("style");
		//$('#top-section .cover-text').css('margin-right','6%');
		$('section').css('margin-left', '0%');
		$('footer').css('margin-left', '0px');
	});
	
	// set up initial positions
	var nav_margin = $('#top-section').height()+'px';
	if (nav_margin > '0px') {
		//console.log(nav_margin);
		//console.log('not zero');
		$('#navigation').css('margin-top', nav_margin );
		$( ".sub-menu" ).animate({top: "-30"}, 10, function() {	});
	} else {
		//console.log('zero');
		//console.log(nav_margin);
		$('#navigation').css('margin-top', '0px' );
		$('.toggle-menu.top-toggle').addClass("fixed");
		$( ".sub-menu" ).animate({top: "80"}, 10, function() {	});
	}
	
	
	$(".faqs > ul > li").addClass("no-active");
		
	$(".faqs > ul > li").click(function(){
		if($(this).hasClass("no-active")){
			$(this).find("div").slideDown();
			$(this).removeClass("no-active");
			$(this).find("h3 span").html("-");
		}
		else {
			$(this).find("div").slideUp();
			$(this).addClass("no-active");
			$(this).find("h3 span").html("+");
		}
	});	
	
	
	
	$(".sub-menu .hide").click(function(e){
		e.preventDefault();
		$( ".sub-menu" ).animate({top: "50"}, 500, function() {
			// Animation complete.
			$( ".sub-menu .show" ).animate({top: "30"}, 500, function() {
				// Animation complete.
			});
		});
		
	});
	
	$(".sub-menu .show").click(function(e){
		e.preventDefault();
		$( ".sub-menu .show" ).animate({top: "-30"}, 500, function() {
			// Animation complete.
			$( ".sub-menu" ).animate({top: "80"}, 500, function() {
				// Animation complete.
			});
		});
	});
	
	
	
	var owl = $("#owl-logos");

    owl.owlCarousel({

    // Define custom and unlimited items depending from the width
    // If this option is set, itemsDeskop, itemsDesktopSmall, itemsTablet, itemsMobile etc. are disabled
    // For better preview, order the arrays by screen size, but it's not mandatory
    // Don't forget to include the lowest available screen size, otherwise it will take the default one for screens lower than lowest available.
    // In the example there is dimension with 0 with which cover screens between 0 and 450px
        
    itemsCustom : [
    	[0, 2],
        [450, 4],
		[800, 4],
        [1000, 6],
        [1200, 8],
		[1920, 10]
  	],
    navigation : false

    });
	
	
	
	
	$(window).scroll(function(){
		var windowScroll = $(window).scrollTop();
		var top_sec_height = $('#top-section').height();
		var para ='-'+((windowScroll)/2)+'px';
		//console.log('windowScroll: '+windowScroll);
		//console.log('top_sec_height: '+top_sec_height);
		
		if (windowScroll > top_sec_height) {
			$('#navigation').removeAttr('style');
			$('#navigation').css({"position": "fixed","left": 0,"top": 0});
			$('#navigation').addClass("fixed-navigation");
			//$('.slider-home').addClass("fixed");
			$('#top-section').removeClass('fixed');
			$('#top-section').css({"top": 0});
			//$('#navigation a.toggle-menu').css("display","block");
			$('a.toggle-menu').addClass('fixed');
			$('.empty-space').css({"width": "100%","height": "80px"});
			//$( ".sub-menu" ).animate({top: "80"}, 100, function() {	});
		}
		else if (windowScroll < top_sec_height){
			$('#navigation').removeAttr('style');
			$('#navigation').removeAttr("class");
			$('#top-section').addClass('fixed');
			$('#top-section').css({"top": para});
			var nav_margin = $('#top-section').height()+'px';
			$('#navigation').css('margin-top', nav_margin );
			//$('#navigation a.toggle-menu').css("display", "none");
			$('a.toggle-menu').removeClass('fixed');
			$('.empty-space').css({"width": "100%","height": "0px"});
			//$( ".sub-menu" ).animate({top: "-30"}, 10, function() {	});
		};
		
	});
	
	$(document).keyup(function(e){
		
		if(e.keyCode === 27) {
			$('a.close').trigger('click');
			$('#nav-close').trigger('click');
	
		}
	});
		
});

$(document).ready(function(){
	
	if ($('ul li.tpl').length > 0) { 
		$('ul li.tpl').matchMaxHeight();
	}
	
	if ($('.childpage-resize').length > 0) { 
		$('.childpage-resize').matchMaxHeight();
	}
	
	if ($('ul.rboxes li').length > 0) { 
		$('ul.rboxes li').matchMaxHeight();
	}	
		
	for(var i=0; i<10; i++) {
		var cur = '.childpage-resize-'+i+'';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	
	
	if ($('.flexslider .slides li').length > 0) { 
		$('.flexslider').flexslider({
			animation: "slide"
	  	});
	}
	
	if ($('.slider-testimonial .slides li').length > 0) { 
		$('.slider-testimonial').flexslider({
			animation: "fade",
			controlNav: true,
			directionNav: false
	  	});
	}
	
	

	
	
});

$(window).resize(function(){

	if ( $( '#navigation' ).hasClass( "fixed-navigation" ) ) {
		// is already fixed 
	} else {
		// not fixed
		var nav_margin = $('#top-section').height()+'px';
		$('#navigation').css('margin-top', nav_margin );
	}
	
	if ($('ul li.tpl').length > 0) { 
		$('ul li.tpl').matchMaxHeight();
	}

	if ($('ul.rboxes li').length > 0) { 
		$('ul.rboxes li').matchMaxHeight();
	}
	
	if ($('.childpage-resize').length > 0) { 
		$('.childpage-resize').matchMaxHeight();
	}
	
	for(var i=0; i<10; i++) {
		var cur = '.childpage-resize-'+i+'';
		if ($(cur).length > 0) { 
			$(cur).matchMaxHeight();
		}
	}
	

});


//plugin to match all heights equal to max height in selection
(function ($) {
	$.fn.matchMaxHeight = function () { 
		var items = $(this);
		$(items).attr('style', ''); 
		$(items).css({});
		var max = 0;
		for(var i=0; i<items.length ; i++){
			max = max < $(items[i]).height() ? 
			$(items[i]).height() : max;
		}
		$(items).css({'display': 'block', 'height': ''+max+'px'});
	}
})(jQuery);