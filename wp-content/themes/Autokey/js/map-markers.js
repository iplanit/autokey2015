/* ==========================================================================
   Map Setup
   ========================================================================== 
*/

var map;

function initialize() {

    var mapOptions = {
      center: new google.maps.LatLng(23.3029,39.1437),
      zoom:3,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      scrollwheel:false,
      disableDefaultUI: true
    };
    
    map = new google.maps.Map(document.getElementById("stockists-map"),
        mapOptions);     
	
	//Zoom Control Container Div
	var zoom_controls = document.createElement('div');
	zoom_controls.id = 'stockists-map-controls';
	
	//Zoom in Control
	var zoom_in_control = document.createElement('a');
	zoom_in_control.className = 'stockists-map-zoom-btn';
	zoom_in_control.id = 'stockists-map-zoom-in';
	
	//Zoom out Control
	var zoom_out_control = document.createElement('a');
	zoom_out_control.className = 'stockists-map-zoom-btn';
	zoom_out_control.id = 'stockists-map-zoom-out';
	
	//Add Zoom Controls to Zoom Control Container
	zoom_controls.appendChild(zoom_in_control);
	zoom_controls.appendChild(zoom_out_control);

	map.controls[google.maps.ControlPosition.TOP_LEFT].push(zoom_controls);     
	
	google.maps.event.addDomListener(zoom_in_control, 'click', function() {
	    var current_zoom = map.getZoom();
	    var new_zoom;
		if(current_zoom < 6) {
	    	new_zoom = current_zoom + 1;
		    map.setZoom(new_zoom);
	    }
    });
    
    google.maps.event.addDomListener(zoom_out_control, 'click', function() {
	    var current_zoom = map.getZoom();
	    var new_zoom;
		if(current_zoom > 2) {
	    	new_zoom = current_zoom - 1;
		    map.setZoom(new_zoom);
	    } 
    });
	loadMarkers();
}

function loadMarkers(){

	var stockist_info = document.createElement("div");
	stockist_info.className = 'stockist-info';
 
	var myOptions = {
	         content: stockist_info
	        ,disableAutoPan: false
	        ,maxWidth: 0
	       	//,pixelOffset: new google.maps.Size(-95, -440)
	        ,zIndex: null
	       	,boxClass: 'stockist-info'
	        ,closeBoxMargin: '8px 8px 0px 0px'
	        //,closeBoxURL: template_dir + '/images/bg-info-window-close-btn.png'
	        //,infoBoxClearance: new google.maps.Size(1, 1)
	        ,isHidden: false
	        ,pane: "floatPane"
	        ,enableEventPropagation: false
	};
	
	var ib = new InfoBox(myOptions); 
    
	for (var i = 0; i < locations.length; i++) {  
		var marker = new google.maps.Marker({
	    	position: locations[i].latlng,
	    	map: map,
	    	icon : template_dir + '/images/marker.png'
		});
		
		marker.setAnimation(google.maps.Animation.DROP);
				
		google.maps.event.addListener(marker, 'click', (function(marker, i) {
		 return function() {
		  
		  	var address = locations[i].address.replace(/(<([^>]+)>)/ig,"");
		  	var info_content = document.createElement("div");
		    info_content.content = '<div class="stockist-info-top"><h4>'+locations[i].title +'</h4><p class="small-text">' + locations[i].address +'</p></div>';
			
			ib.setContent(info_content.content);
			ib.open(map,marker);
		  }
		})(marker, i));
	}
}

