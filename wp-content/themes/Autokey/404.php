<?php get_header(); ?>

<!-- navigation start -->
		<section id="navigation">
			<div class="container">

            <ul class="tabs-navigation left">
				
                <li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                
            </ul>
            
            <ul class="tabs-navigation right">
                <!--<li><a class="scroll-down" href="#map">Contact</a></li>-->
				
				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
			</ul>
            
			<?php
			/*
			<a class="toggle-menu" href="javascript: void(0);"><small>Menu</small> <span></span></a>
			*/
			?>
            </div>
		</section>
		<!-- navigation end -->

<section class="ptb120">

<div class="container">
	<main role="main">
	<!-- section -->
	<section>

	

          <!-- article -->
          <article id="post-404">

              <h1><?php _e( 'Page not found', 'html5blank' ); ?></h1>
              <h2>
                  <a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'html5blank' ); ?></a>
              </h2>

          </article>
          <!-- /article -->


	

	</section>
	<!-- /section -->
	</main>
</div>
</section>
<?php get_footer(); ?>
