<?php 
/**
Template Name: News Page
**/
get_header(); 
?>

	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section" class="fixed" style="margin-top:0px"></section>	
	<?php } ?>
	<!-- top section end -->
		

		
		<!-- navigation start -->
		<section id="navigation">
			<div class="container">

            <ul class="tabs-navigation left">
				
                <li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                
            </ul>
            
            <ul class="tabs-navigation right">
							<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
				<!--<li><a class="scroll-down" href="#testimonial-page"><?php the_title(); ?></a></li>
				<?php
                // $sub_content  =  get_field('sub_content' ,$post->ID);
				// if($sub_content) { 
					// $el = 0;
					// foreach($sub_content as $sub) {
						// $scroll_id =  strtolower($sub['menu_title']);
						// $scroll_id = str_replace(' ','_',$scroll_id);
						// $scroll_id = 'item-'.$el;
						// $el++;
						// $titledisplay = ($sub['menu_title'])? $sub['menu_title'] : $sub['title'];
						// $add2nav = $sub['attach_to_top_nav'];
						?>
						
                        <?php //if ($add2nav) { ?>
						<li><a class="scroll-down" href="<?php //echo '#'.$scroll_id; ?>"><?php //echo $titledisplay; ?></a></li>
			  			<?php //} ?>
                        
			  		<?php //}
             	//} ?>
                <li><a class="scroll-down" href="#map">Contact</a></li>-->
			</ul>
            
			<?php
			/*
			<a class="toggle-menu" href="javascript: void(0);"><small>Menu</small> <span></span></a>
			*/
			?>
            </div>
		
	</section>
    <div class="sub-navigation">
    	<div class="container">
    		<div class="left"><?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?></div>
            <div class="right"><?php dynamic_sidebar('phone-number'); ?></div>
        </div>
        <a href="#" class="bttn hide" title="Close">X</a>
        <a href="#" class="bttn show" title="Show">\/</a>
    </div>
    

	<!-- navigation end -->
	
    <div class="empty-space"></div>


	
	
			<section id="news-page" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?> >
			<div class="container">

			<?php
			// WP_Query arguments
                      $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
$new_query = new WP_Query();
						
                        // The Query
                        $my_query = new WP_Query( 'showposts=2&cat=2053&paged='.$paged);
                        while ( $my_query->have_posts() ) : $my_query->the_post();
                        ?>
                        <div class="blogpost">
						<div class="blogimage">
							<?php
								$image_ch = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' );
								if ($image_ch) { ?>
							<a class="" href="<?php the_permalink(); ?>"><img alt="<?php the_title(); ?>" title="<?php the_title(); ?>" src="<?php echo $image_ch[0];?>" /></a>
							<?php } ?>				
						</div>
						<div class="blogtext">
							<h3 class="news-title"><a href="<?php the_permalink() ?>" rel="bookmark"><?php the_title(); ?></a></h3>
							<?php
								$ex = get_field('excerpt', $post->ID);
								$link_title = get_field('link_title', $post->ID)? get_field('link_title', $post->ID) : 'Read More';
								if ($ex) {
									$ex = $ex;
								} else {
									$ex = substr(get_the_content(), 0,200);
								}
								$readmore = "...";
								$html = strip_tags($ex);
								echo '<p>'.$html.$readmore.'</p>';  
							?>
							<div class="blogbutton">
								<p class="btn-bottom"><a class="large-color-button" href="<?php the_permalink(); ?>"><?php echo $link_title; ?></a></p>
							</div>
							
						</div>
						
					</div>
					
					<div class="clear"></div>
					
                        <?php
                        endwhile;
                        //wp_reset_postdata();	// avoid errors further down the page
                        
						
						
				// pager
if($my_query->max_num_pages>=1){?>
<div class="blogbutton-2">
<h4 class="pagetitle">Read previous articles</h4>
   <p class="btn-bottom-2">
    <?php
    for($i=1;$i<=$my_query->max_num_pages;$i++){?>
        <a href="http://www.autokey.ie/news/<?php echo get_category_link($category_id).'page/'.$i;?>" class="large-color-pager"':'';><?php echo $i;?></a>
        <?php
    }
     ?>
    </p>
	</div>
<?php } ?>
			
					

              </div>
		
		</section>
	


<?php get_footer(); ?>
