<?php
/**
Template Name: Clients Page
**/
get_header(); 
?>
<?php
while( have_posts() ) : the_post();
?>
        <!-- top section -->
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
		if ($image) {
		?>
        
		<section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
                $title = get_field('banner_title' ,$post->ID);
            	$c_text = get_field('rich_text');
				$link = get_field('button_link' ,$post->ID);
				$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>               
			</div>
            <?php } ?>
		</section>
        <?php } ?>
		<!-- top section end -->
		

		
		<!-- navigation start -->
		<section id="navigation">
			<div class="container">

            <ul class="tabs-navigation left">
				<?php /*
                <li><a class="logo-item" href="#top-section"><img src="<?php echo get_bloginfo('template_url'); ?>/images/reconcile-engineering.png" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                */ ?>
                <li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                
                
            </ul>
            
            <ul class="tabs-navigation right">
			
				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
				<?php /*<li><a class="" href="<?php echo home_url(); ?>">Homepage</a></li> */ ?>
                <!--<li><a class="scroll-down" href="#content"><?php //the_title(); ?></a></li>
				<?php
                // $sub_content  =  get_field('sub_content' ,$post->ID);
				// if($sub_content) { 
					// $el = 0;
					// foreach($sub_content as $sub) {
						// $scroll_id =  strtolower($sub['menu_title']);
						// $scroll_id = str_replace(' ','_',$scroll_id);
						// $scroll_id = 'item-'.$el;
						// $el++;
						// $titledisplay = ($sub['menu_title'])? $sub['menu_title'] : $sub['title'];
						// $add2nav = $sub['attach_to_top_nav'];
						?>
						
						<?php //if ($add2nav) { ?>
						<li><a class="scroll-down" href="<?php //echo '#'.$scroll_id; ?>"><?php //echo $titledisplay; ?></a></li>
			  			<?php //} ?>
                
			  		<?php //}
             	//} ?>
                <li><a class="scroll-down" href="#map">Contact</a></li>-->
			</ul>
            
			<?php
			/*
			<a class="toggle-menu" href="javascript: void(0);"><small>Menu</small> <span></span></a>
			*/
			?>
            </div>
		</section>
		<!-- navigation end -->
		
        <!-- page content -->
        
        <section id="content" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
			<div class="container">
				<div class="col-content">
            		<h2><?php the_title(); ?></h2>
					<?php the_content(); ?>
				</div>
			</div>
            
            <div class="container">
            <?php
			  $clients = get_field('logos', $post->ID);
			  if($clients) { 
				  echo '<ul class="c-logos">';
				  foreach($clients as $logo){
						$t = $logo['title'];
						$img = $logo['image']['sizes']['clients-small'];
					  	$in = $logo['about'];
					  
					  	$link = $logo['add_link'];
						$link_t = ($logo['link_title'])? $logo['link_title']: "Read More ";
						$add_link = 'false';
					  	if ($link == 'internal') {
							$add_link = 'true';
							$where = $logo['link_internal'];
							$target = "";
						}
						else if ($link == 'external') {
							$add_link = 'true';
							$where = $logo['link_external'];
							$target = "target='_blank'";	
						}
						else {
							$add_link = 'false';	
						}
						
					  // print
					  
					  echo '<li class="tpl">';
					  		if ($img) {
								// if image	
								// if link 
								if ($add_link=='true') { echo "<a title='".$t."' href='".$where."' ".$target.">"; }
								echo "<img src='".$img."' alt='".$t."' />";
								if ($add_link=='true') { echo "</a>"; }
							}
							echo '<h4>'.$t.'</h4>';
							if ($in) {		
								// info
								echo '<p>'.$in.'</p>';
							}

							if ($add_link=='true') {
								// if link
								echo "<p><a title='".$t."' ".$target." href='".$where."' class='links left'>".$link_t." <span class='fa fa-angle-right'></span></a></p>";
							}
					  echo '</li>';
					  
				  }
				  echo '</ul>';
			  } ?>
            </div>
            
		</section>
		
		<?php 
		if($sub_content)
		{
			$el = 0;
		foreach ($sub_content  as $content)
		{
		$scroll_id =  strtolower($content['menu_title']);
		$scroll_id = str_replace(' ','_',$scroll_id);
		$scroll_id = 'item-'.$el;
		$el++;
		
		$background_color = $content['background_color'];
		$image_aligment = $content['image_aligment'];
		$add_sub_pages = $content['add_sub_pages'];
		if($image_aligment == 'left')
			{
			$content_aligment = 'right';
			}
			else
			{
			$content_aligment ='left';
			}
		
		?>
		<section id="<?php echo $scroll_id; ?>" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
			<div class="container">
				<?php if ($content['image_']['url']) { ?>
                <div class="col-half <?php echo $image_aligment; ?>">
				<figure class="<?php echo 'align_'.$image_aligment; ?>">
					<img src="<?php echo $content['image_']['url']; ?>" alt="<?php echo $content['title']; ?>">
                </figure>
				</div>
				<div class="col-half <?php echo $content_aligment; ?>">
					<h2><?php echo $content['title']; ?></h2>
					<?php echo $content['content']; ?>
					
					<?php 
					$buttons  = $content['buttons'];
					if($buttons)
					{ 
					foreach($buttons as $btn){
				
					?>
					<a class="button-link" href="<?php echo $btn['button_text']; ?>"> <?php echo $btn['button_label']; ?></a>
					<?php }
					}
					?>
				</div>
                <?php } else { ?>
                <div class="col-full">
					<h2><?php echo $content['title']; ?></h2>
					<?php echo $content['content']; ?>
					
					<?php 
					$buttons  = $content['buttons'];
					if($buttons)
					{ 
					foreach($buttons as $btn){
				
					?>
					<a class="button-link" href="<?php echo $btn['button_text']; ?>"> <?php echo $btn['button_label']; ?></a>
					<?php }
					}
					?>
				</div>
                <?php } ?>
                
			</div>
		</section>
		<?php if($add_sub_pages){
				if($content['sub_pages_list']){
					?>
					<section id="feature-section" class="pbt120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
			<div class="container">
				<?php $arraySize = sizeof($content['sub_pages_list']);
					if($arraySize == 1){$arraySize = 1;}
					if($arraySize == 2){$arraySize = 2;}
					if($arraySize >= 3){$arraySize = 3;}
				?>
				<ul class="<?php echo 'col-'.$arraySize;?>">
					<?php
					foreach($content['sub_pages_list'] as $subPages){
					?>
					
					<li>
						<figure><img src="<?php echo $subPages['sub_icon_image']['url']; ?>" alt="<?php echo $subPages['sub_title']; ?>"></figure>
						<h4 class="mtb15"><?php echo $subPages['sub_title']; ?></h4>
						<p><?php echo $subPages['sub_content']; ?></p>
						<a href="<?php echo $subPages['sub_link']; ?>" class="links"><?php echo $subPages['sub_link_title']; ?><span class="fa fa-angle-right"></span></a>
					</li>
					<?php }
					?>
				</ul>
			</div>
		</section>
				<?php }
			}?>
		
		<?php }  }?>
		


<?php endwhile;  ?>
<?php get_footer(); ?>