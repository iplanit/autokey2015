<?php 
/**
Template Name: Testimonial Page
**/
get_header(); 
?>

	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section" class="fixed" style="margin-top:0px"></section>	
	<?php } ?>
	<!-- top section end -->
		

		
		<!-- navigation start -->
		<section id="navigation">
			<div class="container">

            <ul class="tabs-navigation left">
				
                <li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                
            </ul>
            
            <ul class="tabs-navigation right">
							<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>

				<!--<li><a class="scroll-down" href="#testimonial-page"><?php the_title(); ?></a></li>
				<?php
                // $sub_content  =  get_field('sub_content' ,$post->ID);
				// if($sub_content) { 
					// $el = 0;
					// foreach($sub_content as $sub) {
						// $scroll_id =  strtolower($sub['menu_title']);
						// $scroll_id = str_replace(' ','_',$scroll_id);
						// $scroll_id = 'item-'.$el;
						// $el++;
						// $titledisplay = ($sub['menu_title'])? $sub['menu_title'] : $sub['title'];
						// $add2nav = $sub['attach_to_top_nav'];
						?>
						
                        <?php //if ($add2nav) { ?>
						<li><a class="scroll-down" href="<?php //echo '#'.$scroll_id; ?>"><?php //echo $titledisplay; ?></a></li>
			  			<?php //} ?>
                        
			  		<?php //}
             	//} ?>
                <li><a class="scroll-down" href="#map">Contact</a></li>-->
			</ul>
            
			<?php
			/*
			<a class="toggle-menu" href="javascript: void(0);"><small>Menu</small> <span></span></a>
			*/
			?>
            </div>
		
	</section>
    <div class="sub-navigation">
    	<div class="container">
    		<div class="left"><?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?></div>
            <div class="right"><?php dynamic_sidebar('phone-number'); ?></div>
        </div>
        <a href="#" class="bttn hide" title="Close">X</a>
        <a href="#" class="bttn show" title="Show">\/</a>
    </div>
    

	<!-- navigation end -->
	
    <div class="empty-space"></div>


	
	
			<section id="testimonial-page" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?> >
			<div class="container">
		      <h2><?php the_title(); ?></h2>
				
				<?php
				/*
				<ul class="row">
					<?php query_posts('post_type=testimonials'); 
					while( have_posts() ) : the_post();
					?>
					<li>
						<figure><?php the_post_thumbnail('t-home-image'); ?></figure>
						<h4><?php the_title(); ?></h4>
						<p class="main-para center"><?php the_field('author_post'); ?></p>
						<div class="minimum-height">
						<?php $string = get_the_content($post->ID);
							$string = substr($string,0,175).'';
							echo '<p>'.strip_tags($string).'(...)</p>';?></div>
						<a href="<?php the_permalink(); ?>" class="links right">learn more <span class="fa fa-angle-right"></span></a>
					</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
                */
				?>
                
                <ul class="row testimonial-tpl">
					<?php query_posts('post_type=testimonials'); 
					while( have_posts() ) : the_post();
					?>
					<li>
						<figure>
							<?php the_post_thumbnail('t-home-image'); ?>
                        	
                        </figure>
                        
						
						<h4><?php the_title(); ?></h4>
							<p class="main-para"><?php the_field('author_post'); ?></p>
						<?php $string = get_the_content($post->ID);
                        //$string = substr($string,0,175).'';
						echo '<p>'.strip_tags($string).'</p>';?>
                  	</li>
					<?php endwhile; wp_reset_query(); ?>
				</ul>
			</div>
		</section>
	


<?php get_footer(); ?>
