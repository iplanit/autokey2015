<?php
/**
Template Name: Map Page Template
**/ 
get_header('map'); 
?>
<?php
while( have_posts() ) : the_post();
?>
        <!-- top section -->
		<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); ?>
		<section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
                $title = get_field('banner_title' ,$post->ID);
            	$c_text = get_field('rich_text');
				$link = get_field('button_link' ,$post->ID);
				$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>              
			</div>
            <?php } ?>
		</section>
		<!-- top section end -->
		

		
		<!-- navigation start -->
		<section id="navigation" style="position:fixed;" class="fixed-navigation">
			<div class="container">
            	<ul class="tabs-navigation left">
                	<li><a class="logo-item " href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
                </ul>
            
               <ul class="tabs-navigation right">
				
                <!--<li><a class="scroll-down" href="#contact-map">Contact</a></li>-->
								<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
				
			</ul>
            
            </div>
		</section>
		<!-- navigation end -->
		
		
        
	<!-- map here -->
    <section>
    	<div id="stockists-map" class="map">
    </section>    
		
	<!-- end map -->


<?php endwhile;  ?>
<?php get_footer('map'); ?>