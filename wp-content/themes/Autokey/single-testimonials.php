<?php
get_header(); 
?>

	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section"></section>	
	<?php } ?>
	<!-- top section end -->
		

		
	<!-- navigation start -->
	<section id="navigation">
		<div class="container">
            <ul class="tabs-navigation left">
				<li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
         	</ul>
            
            <ul class="tabs-navigation right">
			
				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
            	<?php //if ($display_content) {?>
				<!--<li><a class='scroll-down' href='#content'><?php the_title(); ?></a></li>-->
                <?php //} ?>
                <!-- <li><a class="scroll-down" href="#map">Contact</a></li>-->
			</ul>
        
       	</div>
	 
    
    
    	</section>
    <div class="sub-navigation">
    	<div class="container">
    		<div class="left"><?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?></div>
            <div class="right"><?php dynamic_sidebar('phone-number'); ?></div>
        </div>
        <a href="#" class="bttn hide" title="Close">X</a>
        <a href="#" class="bttn show" title="Show">\/</a>
    </div>
    

	<!-- navigation end -->
	
    <div class="empty-space"></div>
    
    
    	
 
      <section id="content" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
          <div class="container">
              <div class="col-content">
  
				<?php if (have_posts()): while (have_posts()) : the_post(); ?>
    
                  <div style="position:relative">
                  
                    <div class="slider-testimonial">
                      <ul class="slides">
                      <?php $item = $post; { ?>
                     
                       <li>
                          <div class="t-content">
                            <?php $string = $item->post_content;
                            //$string = substr($string,0,175).'';
                            echo '<p>'.$string.'</p>';?>
                          </div>                    
                            <figure>
                                <?php $img = wp_get_attachment_image_src( get_post_thumbnail_id( $item->ID ), 't-home-image'); ?> 
                                <img alt="<?php echo $item->post_title; ?>" class="attachment-t-home-image wp-post-image" src="<?php echo $img[0]; ?>">
                                <h4><?php echo $item->post_title; ?></h4>
                                <p class="main-para center"><?php the_field('author_post',$item->ID); ?></p>
                            </figure>

                      </li>
                    <?php } ?>       
                    </ul>
                  </div>

              </div>
    

		<?php endwhile; ?>

		<?php else: ?>

            <!-- article -->
            <article>
                <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
            </article>
            <!-- /article -->

	<?php endif; ?>

            
            
            
            
            


              </div>
          </div>
      </section>
	



<?php get_footer(); ?>
