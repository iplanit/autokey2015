<!-- footer start -->
	<footer>
		<div class="sitemap" id="contact-map">
			<div class="container">
				<ul class="clearfix">	
					<li>
						<?php dynamic_sidebar('footer-1'); ?>
					</li>
					<li>
						<?php dynamic_sidebar('footer-2'); ?>
					</li>
					<li>
						<?php dynamic_sidebar('footer-3'); ?>
					</li>
					<li class="vcard">
						<?php dynamic_sidebar('footer-4'); ?>
					</li>
				</ul>
			</div>
		</div>
		<section class="footer-bottom">
			<div class="container">
				<?php dynamic_sidebar('footer-developer'); ?>
			</div>
		</section>
	</footer>
<!-- footer end -->
	
</div><!-- / container end here-->	
	  <?php if($_GET['page'] !=''){
	?>
	<script>
		jQuery('html, body').animate({scrollTop: (jQuery('#<?php echo $_GET['page']; ?>').offset().top)}, 800);
		//window.history.pushState('page2', 'Title', '<?php echo get_bloginfo('url'); ?>');
	</script>
	<?php }
	?>
<?php wp_footer(); ?>	
	</body>
</html>
