<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo('charset'); ?>">
		<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' |'; } ?> <?php bloginfo('name'); ?></title>
		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_bloginfo('template_url'); ?>/favicon.ico" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800,300' rel='stylesheet' type='text/css'>
                <?php wp_head(); ?>
		<script type="text/javascript" src="//maps.googleapis.com/maps/api/js?key=AIzaSyBfN1K0e7uipv3tZxXhDidM4e7YyM0zNAY&sensor=false"></script>
    	<script type="text/javascript" src="//google-maps-utility-library-v3.googlecode.com/svn/trunk/infobox/src/infobox.js"></script>
		<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/map-markers-home.js"></script>	
	
    
    	<?php
			$markers = "";
			$locations = get_field('locations',2);
			foreach($locations as $l) {
				$lat = "latlng : new google.maps.LatLng(".$l['location']['lat'].",".$l['location']['lng'].")";
				$title = "title: '".$l['title']."'";
				$address = "address: '".$l['address']."'";
				$markers .="{".$lat.",".$title.",".$address."},";	
			}
			$markers = substr($markers,0,(strlen($markers)-1));
			?>
    
    <script type="text/javascript">
      var template_dir = "<?php echo get_bloginfo('template_url'); ?>";        
			var locations = [<?php echo $markers;?>];
	</script>
    
       <script type="text/javascript">
		google.maps.event.addDomListener(window, 'load', initialize);   
	</script>    
    
    </head>
	<body <?php body_class(); ?>>
	<nav>
    	<div class="black">
        	<h4 class="menu">MENU</h4>
			<ul class='right-navigation'>
				<a id="nav-close" class="nav-close" href="#">x</a>
				<?php dynamic_sidebar('right-1'); ?>
			</ul>
        </div>
        <div class="red">
			<ul class='right-navigation-red'>
				<?php dynamic_sidebar('right-2'); ?>
			</ul>
            <div class="clear"></div>
        </div>
        <div class="clear"></div>
	</nav>
            
	<a class="toggle-menu top-toggle" href="javascript: void(0);"><small>Menu</small> <span></span></a>

<div class="main-container">	
	