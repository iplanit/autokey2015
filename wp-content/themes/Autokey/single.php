<?php
get_header(); 
?>

	<!-- top section -->
	<?php $image = wp_get_attachment_image_src( get_post_thumbnail_id( $post->ID ), 'single-post-thumbnail' ); 
	if ($image) { ?>
        <section id="top-section" style = "background:url('<?php echo $image[0] ?>') no-repeat;background-size:cover; background-position:center;" class="fixed"  >
			<?php
        	$title = get_field('banner_title' ,$post->ID);
            $c_text = get_field('rich_text');
			$link = get_field('button_link' ,$post->ID);
			$b_text = get_field('banner_button_text' ,$post->ID);
			?>
            <?php if ($title || $c_text) { ?>
            <div class="cover-text right">
				<?php if ($title) { echo "<h1>".$title."</h1>"; } ?>
                <?php if ($c_text) { echo "<p>".$c_text."</p>"; } ?>
                <?php if ($link && $b_text) { echo "<a href='".$link."'>".$b_text."</a>"; } ?>                
			</div>
            <?php } ?>
		</section>
    <?php } else {?>
		<section id="top-section"></section>	
	<?php } ?>
	<!-- top section end -->
		

		
	<!-- navigation start -->
	<section id="navigation">
		<div class="container">
            <ul class="tabs-navigation left">
				<li><a class="logo-item" href="<?php echo home_url(); ?>"><img src="<?php echo get_bloginfo('template_url'); ?>/images/logo.gif" alt="<?php echo get_bloginfo( 'name' ); ?>" /></a></li>
         	</ul>
            
            <ul class="tabs-navigation right">

				<?php wp_nav_menu( array('menu' => 'Top Main Navigation' )); ?>
            	<?php //if ($display_content) {?>
				<!-- <li><a class='scroll-down' href='#content'><?php //the_title(); ?></a></li>-->
                <?php //} ?>
                <!--<li><a class="scroll-down" href="#map">Contact</a></li>-->
			</ul>
        
       	</div>
	</section>
	<!-- navigation end -->
	
    <div class="empty-space"></div>
    	

 
      <section id="content" class="ptb120" <?php if($background_color) { echo 'style="background:'.$background_color.'"'; } else { echo 'style="background:#ffffff"';}  ?>>
          <div class="container">
              <div class="col-content">

				  <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                  
                      <!-- article -->
                      <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                  
                          <!-- post thumbnail -->
                          <?php /*
                          <?php if ( has_post_thumbnail()) : // Check if thumbnail exists ?>
                              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                  <?php the_post_thumbnail(array(120,120)); // Declare pixel size you need inside the array ?>
                              </a>
                          <?php endif; ?>
                          */ ?>
                          <!-- /post thumbnail -->
                  
                          <!-- post title -->
                          <h2>
                              <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a>
                          </h2>
                           <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                          <!-- /post title -->
                  
                          <!-- post details -->
                          <?php /*
                          <span class="date"><?php the_time('F j, Y'); ?> <?php the_time('g:i a'); ?></span>
                          <span class="author"><?php _e( 'Published by', 'html5blank' ); ?> <?php the_author_posts_link(); ?></span>
                          <span class="comments"><?php comments_popup_link( __( 'Leave your thoughts', 'html5blank' ), __( '1 Comment', 'html5blank' ), __( '% Comments', 'html5blank' )); ?></span>
                          */ ?>
                          <!-- /post details -->
                  
                          <?php the_content(); ?>
                  
                          <?php /* edit_post_link(); */ ?>
                  
                      </article>
                      <!-- /article -->
                  
                  <?php endwhile; ?>
                  
                  <?php else: ?>
                  
                      <!-- article -->
                      <article>
                          <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>
                      </article>
                      <!-- /article -->
                  
                  <?php endif; ?>

              </div>
          </div>
      </section>
	



<?php get_footer(); ?>
